'use strict';

window.onload = function() {

	var gBlock = document.getElementById('goods-block');
	
	var goods = [
		{
			title: 'meizu', 
			name: 'm3 note', 
			desc: {
				color: 'silver',
				corpus: 'metal',
				camera: '10px',

			}
		},
		{
			title: 'apple', 
			name: 'iPhone 6+',
			desc: {
				color: 'red',
				corpus: 'litiu',
				width: '3ml',
				mass:'25gm',
				battery:'5000V.'
			}
		},
		{
			title: 'RK', 
			name: 'RKphone 9+',
			desc: {
				color: 'red',
				corpus: 'syper-plastik',
				camera:'18px',
				battery:'4500V.'
			}
		},
		{
			title: 'xiaomi', 
			name: 'redmi note 4a',
			desc: {
				color: 'black',
				corpus: 'plastic',
					mass:'25gm'
			}
		},
		{
			title: 'nokia', 
			name: 'nokia pro',
			desc: {
				color: 'white',
				corpus: 'metal'
			}
		},
		{
			title: 'RK-car', 
			name: 'tesla-car',
			desc: {
				color: 'blue',
				corpus: 'plastic',
				speed:'300km'
			}
		},
		{
			title: 'RK-carII', 
			name: 'zybr',
			desc: {
				color: 'black',
				corpus: 'metal',
				speed:'250km',
				model:'sedan'
			}
		},
		
		
	]

	var ul = document.createElement('ul');
	ul.id = 'goods-list';

	for(var i = 0; i < goods.length; i++) {
		var li = document.createElement('li')
		li.innerHTML = "<h2>" + goods[i].title + "</h2><p>" + goods[i].name + "</p>";

		var btn = document.createElement('div');
		btn.innerHTML = "<input type='button' data-name='" + goods[i].title + "' value='заказать'><input type='button' data-name='" + goods[i].title + "' value='добавить в корзину'>";

		if(typeof goods[i].desc == typeof {}) {
			var createDesc = document.createElement('ul');
			for(var key in goods[i].desc) {
				var descLi = document.createElement('li');
				descLi.innerHTML = key + ' : ' + goods[i].desc[key];

				createDesc.appendChild(descLi);
			}
		}

		li.appendChild(createDesc);
		li.appendChild(btn);

		ul.appendChild(li);
	}

	gBlock.appendChild(ul);

	// Обрабатываем кнопку "заказать" и "добавить в корзину"
	document.querySelector('#goods-list').onclick = function(e) {
		var target = e.target;

		if(target.tagName == 'INPUT' && target.value == 'заказать') {
			myAlert("<form id='form-block'>Name: <input id='name' type='text' />Phone: <input id='phone' type='phone' />Email: <input id='email' type='email' /><input id='sumbit' type='button' value='Заказать' /></form>");
			// обрабтка формы
			document.querySelector('#submit').onclick = function() {
				var input_name = document.querySelector('#name');
				var input_phone = document.querySelector('#phone');
				var input_email = document.querySelector('#email');
				var params = 'name=' + input_name.value + '&' + 'phone=' + input_phone.value + '&' + 'email=' + input_email.value;
				ajaxPost(params);
			}
		}

		if(target.tagName == 'INPUT' && target.value == 'добавить в корзину') {
			var a = document.querySelector('#korzina');
			document.querySelector('#korzina-count').innerHTML++;
			a.dataset.arr += target.dataset.name + ' ';
		}
	}

	// обрабатываем кнопку "корзина"
	var a = document.querySelector('#korzina');
	a.onclick = function() {
		myAlert("<p>" + a.dataset.arr + "</p><input type='button' id='openForm' value='Заказать'>");
		var openForm = document.querySelector('#openForm');
		openForm.onclick = function() {
			var alert1 = document.querySelector('.alert');
			document.body.removeChild(alert1);
			myAlert("<form id='form-block'>Name: <input id='name' type='text' />Phone: <input id='phone' type='phone' />Email: <input id='email' type='email' /><input id='sumbit' type='button' value='Заказать' /></form>");
			document.querySelector('#submit').onclick = function() {
				var input_name = document.querySelector('#name');
				var input_phone = document.querySelector('#phone');
				var input_email = document.querySelector('#email');
				var params = 'name=' + input_name.value + '&' + 'phone=' + input_phone.value + '&' + 'email=' + input_email.value;
				ajaxPost(params);
			}
		}
	}

	// funcion alert
	function myAlert(msg) {
		var div = document.createElement("div");
		div.classList.add('alert');
		div.innerHTML = "<button id='close-alert'>[x]</button>" + msg;

		document.body.appendChild(div);

		var btn = document.getElementById('close-alert');

		btn.onclick = function() {
			document.body.removeChild(div);
		}
	}

	// Обработка формы
	function ajaxPost(params) {
		var xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if(xhr.readyState == 4 && xhr.status == 200) {
				if(xhr.respineText == 'ok') {
					alert('Спасибо за заявку !');
				} else {
					alert('Вы что-то ввели не правильно !');
				}
			}
		}

		xhr.open('POST', '../mail.php');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.send(params);
	} 

}