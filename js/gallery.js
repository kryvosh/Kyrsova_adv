'use strict';

window.onload = function() {

	var width = 205;
	var block = 3;


	var img = ['imgs/м1.jpg', 'imgs/м2.jpg', 'imgs/м3.jpg', 'imgs/м4.jpg', 'imgs/м5.jpg', 'imgs/м6.jpg','imgs/м7.jpg','imgs/м8.jpg','imgs/м9.jpg','imgs/м10.jpg','imgs/м11.jpg','imgs/м12.jpg'];
	slider('slider', '.slider-list', '.slider-li', 12, img, '.prev', '.next');


	var img2 = ['imgs/т1.jpg', 'imgs/т2.jpg', 'imgs/т3.jpg', 'imgs/т4.jpg', 'imgs/т5.jpg', 'imgs/т6.jpg','imgs/т7.jpg','imgs/т8.jpg','imgs/т9.jpg','imgs/т10.jpg','imgs/т11.jpg','imgs/т12.jpg'];
	slider('slider2', '#slider-list', '.slider-li', 12, img2, '.prev', '.next');

	var img3 = ['imgs/н1.jpg', 'imgs/н2.jpg', 'imgs/н3.jpg', 'imgs/н4.jpg', 'imgs/н5.jpg', 'imgs/н6.jpg','imgs/н7.jpg','imgs/н8.jpg','imgs/н9.jpg','imgs/н10.jpg','imgs/н11.jpg','imgs/н12.jpg'];
	slider('slider3', '#slider-list1', '.slider-li', 12, img3, '.prev', '.next');

	sliderShow('#btn-g-1', '.slider');
	sliderShow('#btn-g-2', '#slider2');
	sliderShow('#btn-g-3', '#slider3');



	document.querySelector('.slider').onclick = function(e) {
		var target = e.target;

		if(target.tagName == 'IMG') {
			var src = target.getAttribute('src');
			document.querySelector('.gallery').style.display = 'flex';
			bigSlider(src, img);
		}
	}

	document.querySelector('#slider2').onclick = function(e) {
		var target = e.target;

		if(target.tagName == 'IMG') {
			var src = target.getAttribute('src');
			document.querySelector('.gallery').style.display = 'flex';
			bigSlider(src, img2);
		}
	}

	document.querySelector('#slider3').onclick = function(e) {
		var target = e.target;

		if(target.tagName == 'IMG') {
			var src = target.getAttribute('src');
			document.querySelector('.gallery').style.display = 'flex';
			bigSlider(src, img3);
		}
	}



	function bigSlider(countImg, arrImg) {

		var newImg = document.createElement('img');
		var btnPrev = document.querySelector('.gallery-prev');
		var btnNext = document.querySelector('.gallery-next');

		document.querySelector('.gallery').insertBefore(newImg, btnNext);

		
		newImg.setAttribute('src', countImg);
		newImg.classList.add('gallery-img');

		bigGallery('gallery', arrImg);


	}


	function sliderShow(btn, slider) {
		var btn = document.querySelector(btn),
			slider = document.querySelector(slider);

		btn.onclick = function() {
			slider.classList.toggle('show');
		}
	}


	function slider(slider, ul, li, countImg, arrImg, prev, next) {
		var slider = document.getElementById(slider),
			ul = slider.querySelector(ul), 
			li = ul.querySelectorAll(li); 

		var position = 0;

		for(var i = 0; i < countImg; i++) { 
			var arrayImage = new Image();
			arrayImage.src = arrImg[i];
			li[i].appendChild(arrayImage);
			slider.querySelectorAll('img')[i].classList.add('slider-img');
		}

		slider.querySelector(prev).onclick = function() {
			position = Math.min(position + width * block, 0);
			ul.style.marginLeft = position + 'px';
		}

		slider.querySelector(next).onclick = function() {
			position = Math.max(position - width * block, -width * (li.length - block));
			ul.style.marginLeft = position + 'px';
		}

	}

	function bigGallery(slider, arrImg) {
		var position = 0;
		var newImg = document.querySelector('.gallery-img');

		document.querySelector('.gallery-prev').onclick = function() {
			if(position > 0) {
				position--;
			} else {
				position = arrImg.length - 1;
			}

			newImg.setAttribute('src', arrImg[position]);
		}


		document.querySelector('.gallery-next').onclick = function() {

			if(position < arrImg.length - 1) {
				position++;
			} else {
				position = 0;
			}

			newImg.setAttribute('src', arrImg[position]);
		}
	}



	document.querySelector('#closeBigSlider').onclick = function() {
		var div = document.querySelector('#gallery'),
			img = document.querySelector('.gallery-img');

		div.style.display = 'none';
		div.removeChild(img);

	}
}