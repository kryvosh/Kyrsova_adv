'use strict';

window.onload = function() {

	// slider show and hide
	document.querySelector('h1').onclick = function() {
		document.querySelector('#slider').classList.toggle('showHide');
	}

	// slider ---------------------------
	var width = 205;
	var block = 3;

	var slider = document.getElementById('slider'),
		ul = slider.querySelector('ul'),
		li = slider.querySelectorAll('li');

	var position = 0;

	slider.querySelector('.prev').onclick = function() {
		position = Math.min(position + width * block, 0);
		ul.style.marginLeft = position + 'px';
	}

	slider.querySelector('.next').onclick = function() {
		position = Math.max(position - width * block, -width * (li.length - block));
		ul.style.marginLeft = position + 'px';
	}


	// contact ---------------------------
	var contact = document.getElementById('contact');

	contact.onclick = function() {
		var div = document.createElement("div");
		div.classList.add('alert');
		div.innerHTML = "<button id='close-alert'>[x]</button><h2>Контакты !</h2><p>098-890-9889</p><p>contact@gmail.com</p><p>г.Киев ул.Банковая д.№1</p>";

		document.body.appendChild(div);

		var btn = document.getElementById('close-alert');

		btn.onclick = function() {
			document.body.removeChild(div);
		}
	}


	// parallax effect ---------------------------
	var sec = document.querySelector('.parallax');
	sec.onmousemove = function(e) {
		var width = sec.clientWidth,
			height = sec.clientHeight;

		var offsetX = 0.5 - e.clientX / width,
			offsetY = 0.5 - e.clientY / height;

		var parallaxArr = document.querySelectorAll('.parallax-img');
		
		parallaxArr.forEach(function(el, i){
			var offset = parseInt(el.dataset.offset);

			var position = Math.round(offsetY * offset) + 'px ' + Math.round(offsetX * offset) + 'px';

			el.style.margin = position;
		});

	}

}